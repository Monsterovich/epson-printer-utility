#!/bin/sh

for file in $(find . -name "*.h")
do
	base=$(basename -- $file)
    moc_cpp="moc_"$(echo "$base" | sed -e "s/\.h/\.cpp/g")
    echo "Running moc on "$file
    moc $file > $moc_cpp
done

for file in $(find ../EPUCommon/* -name "*.h")
do
    base=$(basename -- $file)
    moc_cpp="moc_"$(echo "$base" | sed -e "s/\.h/\.cpp/g")
    echo "Running moc on "$file
    moc $file > $moc_cpp
done



